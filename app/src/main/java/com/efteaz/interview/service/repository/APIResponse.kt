package com.efteaz.interview.service.repository

/**
 * Created by Md Efteaz on 2019-10-26.
 */
class APIResponse<T> {

    var status: Int? = null
    var data: T? = null
    var error: Throwable? = null

    constructor() {}

    private constructor(requestStatus: Int, data: T?, error: Throwable?) {
        this.status = requestStatus
        this.data = data
        this.error = error
    }

    fun loading(): APIResponse<T> {
        return APIResponse(LOADING, null, null)
    }

    fun success(data: T): APIResponse<T> {
        return APIResponse(SUCCESS, data, null)
    }

    fun error(error: Throwable): APIResponse<T> {
        return APIResponse(ERROR, null, error)
    }

    fun unAuthorized(error: Throwable): APIResponse<T> {
        return APIResponse(UNAUTHORIZED, null, error)
    }

    companion object {
        val LOADING = 0
        val SUCCESS = 1
        val ERROR = -1
        val UNAUTHORIZED = 401
    }
}
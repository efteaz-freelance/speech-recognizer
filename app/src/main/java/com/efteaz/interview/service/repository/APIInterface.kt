package com.efteaz.interview.service.repository

import com.efteaz.interview.utils.Constants
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET

/**
 * Created by Md Efteaz on 2019-10-26.
 */
interface APIInterface {
    @GET(Constants.API_DICTIONARY)
    fun dictionary(): Call<ResponseBody>
}
package com.efteaz.interview.service.model

import java.io.Serializable

/**
 * Created by Md Efteaz on 2019-10-26.
 */

data class DictionaryModel(val word: String, var frequency: Int, var isSelected: Boolean) : Serializable
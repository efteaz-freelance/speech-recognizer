package com.efteaz.interview.service.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.efteaz.interview.service.model.DictionaryModel
import com.efteaz.interview.utils.Constants
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.collections.ArrayList

/**
 * Created by Md Efteaz on 2019-10-26.
 */
object APIRepository {
    val apiInterface = APIClient.retrofit(Constants.BASE_URL)

    //server call to fetch dictionary
    fun dictionary(): LiveData<APIResponse<ArrayList<DictionaryModel>>> {
        val mutableLiveData = MutableLiveData<APIResponse<ArrayList<DictionaryModel>>>()

        mutableLiveData.value = APIResponse<ArrayList<DictionaryModel>>().loading()

        val call = apiInterface.dictionary()
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {

                        val dictionaryArray =
                            JSONObject(response.body().string()).optJSONArray("dictionary")

                        var arrayList = ArrayList<DictionaryModel>()
                        dictionaryArray.let {
                            for (i in 0 until it.length()) {
                                val dictionaryObject = dictionaryArray.optJSONObject(i)
                                val word = dictionaryObject.optString("word")
                                val frequency = dictionaryObject.optInt("frequency")

                                var model = DictionaryModel(word, frequency, false)
                                arrayList.add(model)

                            }
                        }

                        arrayList.sortWith(Comparator { first, second -> second.frequency.compareTo(first.frequency) })
                        mutableLiveData.value =
                            APIResponse<ArrayList<DictionaryModel>>().success(arrayList)
                    } catch (e: Exception) {
                        e.printStackTrace()
                        mutableLiveData.value =
                            APIResponse<ArrayList<DictionaryModel>>().error(Exception(e.message.toString()))
                    }

                } else {
                    mutableLiveData.value =
                        APIResponse<ArrayList<DictionaryModel>>().error(Exception(response.errorBody().string()))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                mutableLiveData.value =
                    APIResponse<ArrayList<DictionaryModel>>().error(Exception(t.message.toString()))
            }
        })

        return mutableLiveData
    }
}
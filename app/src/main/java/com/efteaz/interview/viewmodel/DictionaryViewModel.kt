package com.efteaz.interview.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.efteaz.interview.service.model.DictionaryModel
import com.efteaz.interview.service.repository.APIRepository
import com.efteaz.interview.service.repository.APIResponse

/**
 * Created by Md Efteaz on 2019-10-26.
 */
class DictionaryViewModel : ViewModel() {

    fun dictionary(): LiveData<APIResponse<ArrayList<DictionaryModel>>>? {
        return APIRepository.dictionary()
    }
}
package com.efteaz.interview.utils

/**
 * Created by Md Efteaz on 2019-10-26.
 */
object Constants {
    //base url of api
    const val BASE_URL = "http://a.galactio.com/interview/"

    //api endpoints
    const val API_DICTIONARY = "dictionary-v2.json"

}
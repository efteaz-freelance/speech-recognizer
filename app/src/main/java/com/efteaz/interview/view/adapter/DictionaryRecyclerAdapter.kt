package com.efteaz.interview.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.efteaz.interview.R
import com.efteaz.interview.service.model.DictionaryModel
import kotlinx.android.synthetic.main.item_dictionary.view.*

/**
 * Created by Md Efteaz on 2019-10-26.
 */
class DictionaryRecyclerAdapter(dictionaryList: ArrayList<DictionaryModel>) : RecyclerView.Adapter<DictionaryRecyclerAdapter.ViewHolder>() {
    val dictionaryList = dictionaryList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_dictionary, parent, false
        )
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dictionaryList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dictionaryList[position])
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(dictionaryModel: DictionaryModel) {
            with(itemView) {

                //checking last updated item and setting views according to that
                if(dictionaryModel.isSelected) {
                    clItem.setBackgroundColor(ContextCompat.getColor(context, R.color.colorSelectedWord))
                    tvWord.setTextColor(ContextCompat.getColor(context, R.color.colorWhite))
                    tvFrequency.setTextColor(ContextCompat.getColor(context, R.color.colorWhite))
                } else {
                    clItem.setBackgroundColor(ContextCompat.getColor(context, R.color.colorWhite))
                    tvWord.setTextColor(ContextCompat.getColor(context, R.color.colorBlack))
                    tvFrequency.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
                }
                tvWord.text = dictionaryModel.word
                tvFrequency.text = "${dictionaryModel.frequency}"
            }
        }
    }
}
package com.efteaz.interview.view.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import com.efteaz.interview.R
import android.view.MotionEvent
import android.view.View
import com.efteaz.interview.service.model.DictionaryModel
import kotlinx.android.synthetic.main.activity_voice_recognition.*
import android.app.Activity
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.toolbar.*

class VoiceRecognitionActivity : AppCompatActivity(), View.OnClickListener {

    //dictionaryList to hold dictionary coming from previous page
    private lateinit var dictionaryList: ArrayList<DictionaryModel>

    private lateinit var recognizerIntent: Intent
    private lateinit var recognizer: SpeechRecognizer
    private lateinit var voiceResultList: ArrayList<String>
    private var isSearching = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_voice_recognition)

        initViews()

        //getting dictionaryList from intent
        if(intent.extras != null) {
            dictionaryList = intent.getSerializableExtra("dictionary") as ArrayList<DictionaryModel>
        }

        setupRecognizer()
    }

    override fun onDestroy() {
        super.onDestroy()
        //releasing recognizer
        recognizer.destroy()
    }

    private fun initViews() {
        //showing close button in toolbar
        cvClose.visibility = View.VISIBLE
        tvToolbarTitle.text = getString(R.string.voice_recognition_title)

        //initializing dictionaryList and voiceResultList
        dictionaryList = ArrayList()
        voiceResultList = ArrayList()

        //record card view touch listener to listen tap
        cvRecord.setOnTouchListener { _, event ->
            when(event.action) {
                MotionEvent.ACTION_DOWN -> {
                    if(!isSearching) {
                        isSearching = true
                        tvMessage.text = ""

                        //using animations
                        val fadeOutAnimation = AnimationUtils.loadAnimation(this, android.R.anim.fade_out)
                        fadeOutAnimation.fillAfter = true
                        tvButtonLabel.animation = fadeOutAnimation
                        recognizer.startListening(recognizerIntent)

                        val voiceRecognitionAnimation = AnimationUtils.loadAnimation(this, R.anim.anim_voice_recognition_running)
                        cvRecordAnimation.startAnimation(voiceRecognitionAnimation)
                    } else {
                        manageSearchingUi(isSearching, true, getString(R.string.voice_recognition_error_listener_searching))
                    }
                }
                MotionEvent.ACTION_UP -> {
                    recognizer.stopListening()
                    cvRecordAnimation.clearAnimation()

                    tvMessage.text = ""
                    val fadeInAnimation = AnimationUtils.loadAnimation(this, android.R.anim.fade_in)
                    fadeInAnimation.fillAfter = true
                    tvButtonLabel.animation = fadeInAnimation
                    pbSearching.visibility = View.VISIBLE
                }
            }
            true
        }

        //setting listeners to views
        cvClose.setOnClickListener { onClick(it) }
    }

    //method to perform respective click operations
    override fun onClick(v: View?) {
        when(v?.id) {
            cvClose.id -> {
                onBackPressed()
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            }
        }
    }


    //method to check whether text is available in dictionary or not
    private fun checkDictionary() {
        var isMatched = false
        for(dictionaryModel in dictionaryList) {
            for(text in voiceResultList) {
                if(text.toLowerCase() == dictionaryModel.word.toLowerCase()) {
                    manageSearchingUi(false, false, "")
                    isMatched = true
                    tvMessage.text = dictionaryModel.word

                    //for multiple updates
//                    model.frequency+=1

                    //for single update
                    gotoDictionary(dictionaryModel.word)
                }
            }
        }

        if(!isMatched) {
            manageSearchingUi(false, true, getString(R.string.voice_recognition_error_no_results))
        }
    }

    //method to manage ui of searching
    private fun manageSearchingUi(isSearching: Boolean, isError: Boolean, message: String) {
        cvRecordAnimation.clearAnimation()
        if(!isSearching) {
            pbSearching.visibility = View.GONE
            this.isSearching = isSearching
        }

        if(isError) {
            val snackbar = Snackbar.make(clLayout, message, Snackbar.LENGTH_LONG)
            val sbView = snackbar.view
            sbView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorError))
            snackbar.show()
        }
    }

    //go to dictionary activity after getting matching word
    private fun gotoDictionary(word: String) {
        val intent = Intent()
        //we can send full list if more than one changes
//        intent.putExtra("dictionary", arrayList)

        //to send only single update
        intent.putExtra("word", word)

        setResult(Activity.RESULT_OK, intent)
        finish()
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }


    //setting up voice recognizer
    private fun setupRecognizer() {
        recognizerIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        recognizerIntent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
//        recognizerIntent.putExtra(
//            RecognizerIntent.EXTRA_CALLING_PACKAGE,
//            "com.efteaz.interview"
//        )

        recognizer = SpeechRecognizer
            .createSpeechRecognizer(this.applicationContext)
        val listener = object : RecognitionListener {
            override fun onResults(results: Bundle) {
                voiceResultList = results
                    .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
                if (voiceResultList.isEmpty()) {
                    pbSearching.visibility = View.GONE
                    manageSearchingUi(false, true, getString(R.string.voice_recognition_error_no_results))
                } else {
                    checkDictionary()
                }
            }

            override fun onReadyForSpeech(params: Bundle) {
                println("Ready for speech")
            }

            /**
             * ERROR_NETWORK_TIMEOUT = 1;
             * ERROR_NETWORK = 2;
             * ERROR_AUDIO = 3;
             * ERROR_SERVER = 4;
             * ERROR_CLIENT = 5;
             * ERROR_SPEECH_TIMEOUT = 6;
             * ERROR_NO_MATCH = 7;
             * ERROR_RECOGNIZER_BUSY = 8;
             * ERROR_INSUFFICIENT_PERMISSIONS = 9;
             *
             * @param error code is defined in SpeechRecognizer
             */
            override fun onError(error: Int) {
                when(error) {
                    1 -> manageSearchingUi(false, true, getString(R.string.voice_recognition_error_network_timeout))
                    2 -> manageSearchingUi(false, true, getString(R.string.voice_recognition_error_network))
                    3 -> manageSearchingUi(false, true, getString(R.string.voice_recognition_error_audio))
                    4 -> manageSearchingUi(false, true, getString(R.string.voice_recognition_error_server))
                    5 -> manageSearchingUi(false, true, getString(R.string.voice_recognition_error_client))
                    6 -> manageSearchingUi(false, true, getString(R.string.voice_recognition_error_time_out))
                    7 -> manageSearchingUi(false, true, getString(R.string.voice_recognition_error_no_results))
                    8 -> manageSearchingUi(false, true, getString(R.string.voice_recognition_error_busy))
                    9 -> manageSearchingUi(false, true, getString(R.string.voice_recognition_error_permission))
                }

            }

            override fun onBeginningOfSpeech() {
                println("Speech starting")
            }

            override fun onBufferReceived(buffer: ByteArray) {
                // TODO Auto-generated method stub

            }

            override fun onEndOfSpeech() {
                // TODO Auto-generated method stub
            }

            override fun onEvent(eventType: Int, params: Bundle) {
                // TODO Auto-generated method stub

            }

            override fun onPartialResults(partialResults: Bundle) {
                // TODO Auto-generated method stub

            }

            override fun onRmsChanged(rmsdB: Float) {
                // TODO Auto-generated method stub

            }
        }
        recognizer.setRecognitionListener(listener)

    }

}

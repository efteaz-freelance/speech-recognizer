package com.efteaz.interview.view.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.efteaz.interview.R
import com.efteaz.interview.service.model.DictionaryModel
import com.efteaz.interview.service.repository.APIResponse
import com.efteaz.interview.view.adapter.DictionaryRecyclerAdapter
import com.efteaz.interview.viewmodel.DictionaryViewModel
import kotlinx.android.synthetic.main.activity_dictionary.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlin.Comparator
import kotlin.collections.ArrayList

class DictionaryActivity : AppCompatActivity(), View.OnClickListener {

    //request codes
    private val REQUEST_CODE_VOICE_RECOGNITION = 101
    private val REQUEST_CODE_PREMISSION_RECORD_AUDIO = 102
    private val REQUEST_CODE_PREMISSION_ALL = 103

    //view model
    private lateinit var viewModel: DictionaryViewModel

    //dictionary related variables
    private lateinit var dictionaryList: ArrayList<DictionaryModel>
    private lateinit var dictionaryRecyclerAdapter: DictionaryRecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dictionary)

        //initializing view model
        viewModel = ViewModelProviders.of(this).get(DictionaryViewModel::class.java)

        //requesting all permissions at first time if android version is greater that marshmallow
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(arrayOf(Manifest.permission.RECORD_AUDIO), REQUEST_CODE_PREMISSION_ALL)
        }

        initViews()

        //method to fetch dictionary from server
        dictionary()

    }

    //initializing views
    private fun initViews() {
        //setting title of page
        tvToolbarTitle.text = getString(R.string.dictionaries_title)

        //recyclerview setup for dictionary
        dictionaryList = ArrayList()
        rvDictionary.layoutManager = LinearLayoutManager(this)
        dictionaryRecyclerAdapter = DictionaryRecyclerAdapter(dictionaryList)
        rvDictionary.adapter = dictionaryRecyclerAdapter

        //setting click listeners to ui views
        btnSpeechToText.setOnClickListener { onClick(it) }
    }

    //performing click operations to ui views
    override fun onClick(v: View?) {
        when(v?.id) {
            btnSpeechToText.id -> {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if(checkSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
                        gotoVoiceRecognition()
                    } else {
                        requestPermissions(arrayOf(Manifest.permission.RECORD_AUDIO), REQUEST_CODE_PREMISSION_RECORD_AUDIO)
                    }
                } else {
                    gotoVoiceRecognition()
                }

            }
        }
    }

    //method to open voice recognition activity
    private fun gotoVoiceRecognition() {
        val intent = Intent(this, VoiceRecognitionActivity::class.java)
        intent.putExtra("dictionary", dictionaryList)
        startActivityForResult(intent, REQUEST_CODE_VOICE_RECOGNITION)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }

    //method to check whether permissions granted or not
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE_PREMISSION_RECORD_AUDIO -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    gotoVoiceRecognition()
                }
            }
        }
    }

    //method to get result from intent which is called for result
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == REQUEST_CODE_VOICE_RECOGNITION) {
            if(resultCode == Activity.RESULT_OK) {
                if(data?.extras != null) {
                    //if multiple updates we can retrieve full dictionary with updates
//                    arrayList.clear()
//                    arrayList.addAll(data.getSerializableExtra("dictionary") as List<DictionaryModel>)

                    //if single update
                    val word = data.getStringExtra("word")
                    for(dictionaryModel in dictionaryList) {
                        if(dictionaryModel.word == word) {
                            dictionaryModel.frequency+=1
                            dictionaryModel.isSelected = true
                        } else {
                            dictionaryModel.isSelected = false
                        }
                    }

                    dictionaryList.sortWith(Comparator { first, second -> second.frequency.compareTo(first.frequency) })
                    dictionaryRecyclerAdapter.notifyDataSetChanged()
                }
            }
        }
    }

    //api calls
    private fun dictionary() {
        //observing viewmodel and fetching data from server
        viewModel.dictionary()?.observe(this, Observer {
            when(it.status) {
                APIResponse.LOADING -> {
                    llLoading.visibility = View.VISIBLE
                }
                APIResponse.ERROR -> {
                    llLoading.visibility = View.GONE
                }
                APIResponse.SUCCESS -> {
                    llLoading.visibility = View.GONE

                    //adding dictionary items to recyclerview
                    dictionaryList.clear()
                    dictionaryList.addAll(it.data!!)
                    btnSpeechToText.isEnabled = true
                    dictionaryRecyclerAdapter.notifyDataSetChanged()
                }
            }
        })
    }
}
